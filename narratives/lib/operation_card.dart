import 'package:flutter/material.dart';
import 'operation.dart';


class OperationCard extends StatefulWidget {
  final Operation operation;
  final void Function()? delete;
  final Key key;

  OperationCard({required this.operation, this.delete, required this.key}) : super(key: key);

  @override
  _OperationCardState createState() => _OperationCardState();
}

class _OperationCardState extends State<OperationCard> {
  late TextEditingController indexController;
  late TextEditingController operatorController;
  bool isEditing = false;

  @override
  void initState() {
    super.initState();
    indexController = TextEditingController(text: widget.operation.index);
    operatorController = TextEditingController(text: widget.operation.operator);
  }

  @override
  void dispose() {
    indexController.dispose();
    operatorController.dispose();
    super.dispose();
  }
  

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                setState(() {
                  isEditing = true;
                });
              },
              child: isEditing
                  ? TextField(
                      controller: indexController,
                      style: const TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                      ),
                    )
                  : Text(
                      widget.operation.index,
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
            ),
            const SizedBox(height: 6),
            GestureDetector(
              onTap: () {
                setState(() {
                  isEditing = true;
                });
              },
              child: isEditing
                  ? TextField(
                      controller: operatorController,
                      style: const TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                      ),
                    )
                  : Text(
                      widget.operation.operator,
                      style: const TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                      ),
                    ),
            ),
            const SizedBox(height: 6),
            if (isEditing)
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    isEditing = false;
                    widget.operation.index = indexController.text;
                    widget.operation.operator = operatorController.text;
                  });
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.grey[600],
                  padding: EdgeInsets.fromLTRB(6, 12, 6, 12),
                  textStyle: TextStyle(
                    color: Colors.grey
                  )
                ),
                child: SizedBox(
                  width: 161.6, 
                  child: Center(
                    child: Text(
                      'Save',
                      style: TextStyle(
                        color: Colors.white, 
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
              ),
            if (!isEditing)
              IconButton(
                onPressed: widget.delete,
                icon: Icon(Icons.delete),
                iconSize: 16,
                color: Colors.grey,
              ),
          ],
        ),
      ),
    );
  }
}