import 'package:flutter/material.dart';
import '../main.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String? userEmail = '';

  @override
  void initState() {
    super.initState();
    loadUserProfile();
  }

  void loadUserProfile() {
    final User? currentUser = FirebaseAuth.instance.currentUser;
    userEmail = currentUser?.email ?? '';
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        title: Text(
          'My Profile',
          style: TextStyle(
            color: accentColor
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.grey[900],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Email",
              style: TextStyle(
                color: accentColor,
                fontSize: 20,
              ),
            ),
            SizedBox(height: 10),
            Text(
              userEmail!,
              style: TextStyle(
                color: accentColor,
                fontSize: 18,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
