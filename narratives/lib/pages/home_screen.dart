import 'package:flutter/material.dart';
import '../main.dart';

import 'package:narratives/services/authentication.dart';


class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[900],
        title: Text(
          'Welcome to ConFunk',
          style: TextStyle(
            color: accentColor,
            letterSpacing: 2,
            fontSize: 30,
          ),
        ),
        centerTitle: true,
      ),
/////////////////////////
/////////////////////////////////////
///correctButtons
////////////////////////////////////
////////////////////////
      body:Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width: 55,
            child: IconButton(
              onPressed: (){
                Navigator.pushNamed(context, '/profile');                    
              }, 
              icon: Icon(
                Icons.account_box,//old:account_box_sharp
                color: accentColor,
                size: 40,
              ),
            ),
          ),

          Container(
            width: 55,
            child: IconButton(
              onPressed: (){
                Navigator.pushNamed(context, '/storage');
              }, 
              icon: Icon(
                Icons.cloud,//old:apps_outlined
                color: accentColor,
                size: 40,
              ),
            ),
          ),

          Container(
            width: 55,
            child: IconButton(
              onPressed: (){
                Navigator.pushNamed(context, '/narrative');
              }, 
              icon: Icon(
                Icons.add,//old:add_box_sharp
                color: accentColor,
                size: 45,
              ),
            ),
          ),

          Container(
            width: 55,
            child: IconButton(
              onPressed: (){
                Navigator.pushNamed(context, '/settings');                    
              }, 
              icon: Icon(
                Icons.settings,//old:settings_applications_sharp
                color: accentColor,
                size: 40,
              ),
            ),
          ),
        ],
      ), 



      floatingActionButton: FloatingActionButton(
        /////////////////////////////////////////
        onPressed: () async {
          await Auth().signOut();
        },
        ///////////////////////////////////////////
        backgroundColor: Colors.grey[900],
        child: Icon(
          Icons.logout,
          color: Colors.white,
          size: 30,
          ),
        
      ),
    );
  }
}