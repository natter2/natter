import 'package:flutter/material.dart';
import '../operation.dart';
import '../operation_card.dart';
import '../main.dart';


import 'package:cloud_firestore/cloud_firestore.dart';


class Narrative extends StatefulWidget {
  const Narrative({super.key});

  @override
  State<Narrative> createState() => _NarrativeState();
}


class _NarrativeState extends State<Narrative> {

  List <Operation> narrative =[
    Operation(index: "operation 1", operator: 'company 1'),
    Operation(index: "operation 2", operator: 'company 2'),
    Operation(index: "operation 3", operator: 'company 1'),
  ];

/////////////////////////////////////////////////////////////////////////////
void saveNarrative(String narrativeName) {
    final firestoreInstance = FirebaseFirestore.instance;

    // Convert each operation in the list to a map
    List<Map<String, dynamic>> mapOperations = narrative.map((operation) => operation.toMap()).toList();
    
    // Save the operations under the narrative name in Firestore
    firestoreInstance.collection("narratives").doc(narrativeName).set({
      'operations': mapOperations,
    });
}




//old function - save local -> was with shared preferences
//  void saveNarrative(String narrativeName) async {
//    final prefs = await SharedPreferences.getInstance();
//    
//   // Convert each operation in the list to JSON and then store the list as a string
//   List<String> jsonOperations = narrative.map((operation) => operation.toJson()).toList();
//   prefs.setStringList(narrativeName, jsonOperations);
//  }
//////////////////////////////////////////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],

      appBar: AppBar(
        title: Text(
          'New Narrative',
          style: TextStyle(
            color: accentColor
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.grey[900],






      ),




 
    body: Column(
      children: [
        Expanded(
          child: ReorderableListView.builder(
            itemCount: narrative.length,
            itemBuilder: (context, index) {
              return OperationCard(
                operation: narrative[index],
                delete: () {
                  setState(() {
                    narrative.removeAt(index);
                  });
                },
                key: Key('${narrative[index].index}${narrative[index].operator}'),
              );
            },
            onReorder: (oldIndex, newIndex) {
              setState(() {
                if (oldIndex < newIndex) {
                  newIndex -= 1;
                }
                final Operation item = narrative.removeAt(oldIndex);
                narrative.insert(newIndex, item);
              });
            },
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            width: 80,
            height: 80,
            child: IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.white,
                size: 50,
                shadows: <Shadow>[Shadow(color: Colors.black, blurRadius: 4.0)],
              ),
              onPressed: () {
                setState(() {
                  narrative.add(Operation(index: "New Operation", operator: "New Operator"));
                });
              },
            ),
          ),
        ),
      ],
    ),


            
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              TextEditingController nameController = TextEditingController();

              return AlertDialog(
                title: Text('Save Narrative'),
                content: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    iconColor: accentColor,
                    fillColor: accentColor,
                    focusColor: accentColor,
                    hoverColor: accentColor,
                    labelText: 'Enter a name for the Narrative',
                  ),
                ),
                actions: [
                  ElevatedButton(
                    onPressed: () {
                      print("Save button pressed"); // Debug print
                      String name = nameController.text;
                      saveNarrative(name);
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Save',
                      style: TextStyle(
                        color: Colors.grey[900]
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: accentColor,
                    )
                  ),
                ],
              );
            },
          );
        },
        backgroundColor: accentColor,
        child: Icon(
          Icons.save,
          color: Colors.grey[900],
          size: 30,
        ),
      ),
    );
  }
}
