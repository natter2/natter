import 'package:flutter/material.dart';
import '../main.dart';
import '../operation.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

class Storage extends StatefulWidget {
  const Storage({super.key});

  @override
  State<Storage> createState() => _StorageState();
}

class _StorageState extends State<Storage> {
  List<List<Operation>> narratives = [];
  List<String> narrativeNames = [];

  @override
  void initState() {
    super.initState();
    loadNarratives();
  }

void loadNarratives() async {
    final firestoreInstance = FirebaseFirestore.instance;

    // Fetch all narratives from Firestore
    QuerySnapshot querySnapshot = await firestoreInstance.collection('narratives').get();

    // Iterate through each narrative
    for (var narrative in querySnapshot.docs) {
      // Convert each operation in the narrative to an Operation object
      List<dynamic> operationList = narrative.get('operations') as List<dynamic> ?? [];
      List<Operation> operations = operationList
          .map((operation) => Operation.fromMap(operation as Map<String, dynamic>))
          .toList();

      // Add the loaded operations to the narratives list
      narratives.add(operations);

      // Add the narrative name to the list
      narrativeNames.add(narrative.id);
    }

    // Update the state to reflect the loaded narratives
    setState(() {});
  }




  void deleteNarrative(int index) async {
    // Store the name before deleting it from the local list
    String narrativeName = narrativeNames[index];

    setState(() {
      // Delete from local list
      narrativeNames.removeAt(index);
      narratives.removeAt(index);
    });

    // Delete from Firestore
    final firestoreInstance = FirebaseFirestore.instance;
    await firestoreInstance.collection('narratives').doc(narrativeName).delete();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        title: Text(
          'All Narratives',
          style: TextStyle(
            color: accentColor
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.grey[900],
      ),






      body: SafeArea(
        child: ListView.builder(
          itemCount: narrativeNames.length,
          itemBuilder: (context, index) {
            return ExpansionTile(
              title: Text(
                narrativeNames[index],
                style: TextStyle(color: textColor, fontWeight: FontWeight.w400, letterSpacing: 0.5),
              ),
              children: narratives[index].asMap().entries.map<Widget>((entry) {
                int operationIndex = entry.key;
                Operation operation = entry.value;
                return ListTile(
                  title: Text(
                    '${operationIndex + 1}. ${operation.index}',
                    style: TextStyle(color: Colors.grey[500], letterSpacing: 0.5),
                  ),
                  subtitle: Text(
                    'Operator: ${operation.operator}',
                    style: TextStyle(color: Colors.grey[700]),
                  ),
                );
              }).toList(),
              trailing: IconButton(
                icon: Icon(Icons.delete),
                color: Colors.white,
                onPressed: () {
                  deleteNarrative(index);
                },
              ),
            );
          },
        ),
      ),


    );
  }
}
