import 'package:flutter/material.dart';
import '../main.dart';






class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        title: Text(
          'Settings',
          style: TextStyle(
            color: accentColor
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.grey[900],
      ),
    );
  }
}