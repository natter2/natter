import 'package:flutter/material.dart';
import 'package:narratives/main.dart';
import 'package:narratives/services/authentication.dart';


class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}


class _AuthScreenState extends State<AuthScreen> {
  bool _isLogin = false;
  bool _loading = false;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void handleSubmit() async {
    if (!_formKey.currentState!.validate()) return;
    final email = _emailController.value.text;
    final password = _passwordController.value.text;

    setState(() => _loading = true);

    // Check if is login or register
    if (_isLogin) {
      // This is for signing in
      await Auth().signInWithEmailAndPassword(email, password);
    } else {
      // This is for registering
      await Auth().registerWithEmailAndPassword(email, password);
    }

    setState(() => _loading = false);
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //standard
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[900],
        title: Text(
          'Authentication',
          style: TextStyle(
            color: accentColor,
            letterSpacing: 2,
            fontSize: 30,
          ),
        ),
        centerTitle: true,
      ),


      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                _isLogin ? 'Log In' : 'Register',
                style: TextStyle(color: accentColor, fontSize: 24.0, fontWeight: FontWeight.w800),
                ),

                const SizedBox(height: 20,),

                TextFormField(
                  style: TextStyle(color:  accentColor),
                  controller: _emailController,
                  validator: (value) {
                    if (value ==null || value.isEmpty) {
                      return 'Please enter your email';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.grey),
                    hintText: 'email',
                    focusColor: Colors.black,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: accentColor,
                        width: 2, 
                      )
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey
                      )
                    )
                  ),
                ),

                const SizedBox(height: 20,),

                TextFormField(
                  style: TextStyle(color:  accentColor),
                  controller: _passwordController,
                  obscureText: true,
                  validator: (value) {
                    if (value ==null || value.isEmpty) {
                      return 'Please enter your password';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(

                    hintStyle: TextStyle(color: Colors.grey),
                    hintText: 'password',
                    focusColor: Colors.black,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: accentColor,
                        width: 2, 
                      )
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey
                      )
                    ),
                  ),
                ),

                const SizedBox(height: 40,),

                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: accentColor
                  ),
                  onPressed: () => handleSubmit(), 
                  child: _loading
                    ? const SizedBox(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(
                        color: accentColor,
                        strokeWidth: 2,
                      ),
                    )
                    : Text(_isLogin ? 'Login' : 'Register',
                    style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
                ),

                const SizedBox(height: 10,),

                TextButton(
                  onPressed: () {
                    setState(() {
                      _isLogin = !_isLogin;
                    });
                  },
                  
                  child: Text(
                    _isLogin ? 'Create a new account' : 'I already have an account',
                    style: TextStyle(color: accentColor, letterSpacing: 1.1),
                  ),
                ),
              ],
            )
          ),
        )
      ),

    );
  }
}