
//material
import 'package:flutter/material.dart';

//firebase
import 'package:narratives/services/firebase_options.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

//pages
import 'package:narratives/pages/profile_screen.dart';
import 'package:narratives/pages/settings_screen.dart';
import 'pages/storage_screen.dart';
import 'pages/new_narra_screen.dart';
import 'pages/home_screen.dart';
import 'package:narratives/pages/auth_screen.dart';



//MAIN COLORS
const backgroundColor = Color.fromARGB(255, 255, 255, 255);
const accentColor = Color.fromARGB(255, 243, 227, 6);
const textColor = Colors.white;


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        
            if (snapshot.connectionState == ConnectionState.waiting) {
              return CircularProgressIndicator(); 
            } else if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}'); 
            } else if (snapshot.hasData) {

          return MaterialApp(
            home: snapshot.hasData ? Home() : AuthScreen(),
            onGenerateRoute: (settings) {
              switch (settings.name) {
                case '/home':
                  return MaterialPageRoute(builder: (context) => Home());
                case '/narrative':
                  return MaterialPageRoute(builder: (context) => Narrative());
                case '/storage':
                  return MaterialPageRoute(builder: (context) => Storage());
                case '/profile':
                  return MaterialPageRoute(builder: (context) => Profile());
                case '/settings':
                  return MaterialPageRoute(builder: (context) => SettingsScreen());
                default:
                  return MaterialPageRoute(builder: (context) => Home());
              }
            },
          );
        } else {
          return MaterialApp(
            home: AuthScreen(),
          );
        }
      },
    );
  }
}
