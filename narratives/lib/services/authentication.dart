import 'package:firebase_auth/firebase_auth.dart';


class Auth {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<UserCredential> registerWithEmailAndPassword(String email, String password) async {
    final userCredential = await _auth.createUserWithEmailAndPassword(
      email: email, 
      password: password
    );
    return userCredential;
  }

  Future<UserCredential> signInWithEmailAndPassword(String email, String password) async {
    final UserCredential = await _auth.signInWithEmailAndPassword(
        email: email, 
        password: password
    );
    return UserCredential;
  }

  // New logout method
  Future<void> signOut() async {
    await _auth.signOut();
  }
}
