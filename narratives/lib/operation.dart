
import 'dart:convert';



class Operation {
  String index;
  String operator;

  Operation({required this.index, required this.operator});

  // Convert an Operation object into a map
  Map<String, dynamic> toMap() {
    return {
      'index': index,
      'operator': operator,
    };
  }

  // Convert a map into an Operation object
  static Operation fromMap(Map<String, dynamic> map) {
    return Operation(
      index: map['index'],
      operator: map['operator'],
    );
  }
}

